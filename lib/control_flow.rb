# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  new_arr = ''
  str.each_char { |el| new_arr << el if el == el.upcase }
  new_arr
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid = str.length / 2
  str.length % 2 != 0 ? str[mid] : str[mid - 1] + str[mid]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.chars.inject(0) do |count, ch|
    count += 1 if VOWELS.include?(ch)
    count
  end
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).inject(1) { |total, num| total *= num }
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  new_str = ''
  arr.each_with_index do |el, i|
    new_str << el if i == 0
    new_str << separator + el if i != 0
  end
  new_str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.map.with_index do |el, i|
    if i % 2 != 0
      el.upcase
    else
      el.downcase
    end
  end.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  new_str = []
  str.split.each do |word|
    if word.length >= 5
      new_str << word.reverse
    else
      new_str << word
    end
  end
  new_str.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).reduce([]) do |result, num|
    if num % 15 == 0
      result << "fizzbuzz"
    elsif num % 5 == 0
      result << "buzz"
    elsif num % 3 == 0
      result << "fizz"
    else
      result << num
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  (1..num / 2).select { |n| num % n == 0 }.count == 1
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).reduce([]) {|result, n| num % n == 0 ? result << n : result}
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  (1..num).inject([]) {|arr, n| prime?(n) && num % n == 0 ? arr << n : arr}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = []
  even = []
  arr.each { |n| n % 2 == 0 ? even << n : odd << n}
  odd.length == 1 ? odd[0] : even[0]
end
